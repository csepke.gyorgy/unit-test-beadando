﻿namespace Assignment.Interfaces
{
    using System.Collections.Generic;

    public interface INumberUtils
    {
        List<int> GetDivisors(int number);
        bool IsPrime(int number);
        string EvenOrOdd(int number);
    }
}