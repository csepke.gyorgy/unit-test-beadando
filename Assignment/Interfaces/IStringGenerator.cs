﻿namespace Assignment.Interfaces
{
    using System.Collections.Generic;

    public interface IStringGenerator
    {
        List<string> GenerateEvenOddPairs(int pairCount, int max);
    }
}