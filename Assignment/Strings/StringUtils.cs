﻿/**
 * https://hu.wikipedia.org/wiki/Palindrom
 */
namespace Assignment.Strings
{
    using Assignment.Interfaces;
    using System;
    using System.Linq;
    using System.Text;

    //class StringUtils
    public class StringUtils : IStringUtils
    {
        /// <summary>
        /// Checks if the string passed in is a palindrom or not.
        /// </summary>
        /// <param name="str"></param>
        /// <returns>True if the passed in string is a palindrom, otherwise false.</returns>
        public bool IsPalindrom(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                throw new ArgumentException("Argument cannot be be null or whitespace");
            }

            //string reverse = new StringBuilder(str).ToString().Reverse<char>().ToString();
            string reverse = string.Join(string.Empty, str.Reverse());
            //return str.Equals(str);
            return str == reverse;
        }
    }
}
