﻿namespace Assignment.Test
{
    using Assignment.Interfaces;
    using Assignment.Strings;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class StringGeneratorTests
    {
        Mock<INumberGenerator> numberGeneratorMock;
        IStringGenerator stringGenerator;

        [SetUp]
        public void Setup()
        {
            numberGeneratorMock = new Mock<INumberGenerator>();
            stringGenerator = new StringGenerator(numberGeneratorMock.Object);
        }

        [TestCase(1, 1)]
        [TestCase(3, 10)]
        [TestCase(10, 10)]
        [TestCase(100, 100)]
        public void StringGenerator_GenerateEvenOddPairs_Should_ReturnEvenOddPair_When_ArgumentsArePositive(int pairCount, int max)
        {
            // arrange
            numberGeneratorMock.Setup(g => g.GenerateEven(max)).Returns(max % 2 == 0 ? max : max - 1);
            numberGeneratorMock.Setup(g => g.GenerateOdd(max)).Returns(max % 2 == 1 ? max : max - 1);

            // act
            List<string> result = stringGenerator.GenerateEvenOddPairs(pairCount, max);

            // assert
            Assert.That(result.Count == pairCount);
            Assert.That(result.All(s =>
                int.Parse(s.Split(',')[0]) % 2 == 0 &&
                int.Parse(s.Split(',')[0]) <= max &&
                int.Parse(s.Split(',')[1]) % 2 == 1 &&
                int.Parse(s.Split(',')[1]) <= max));
        }

        [TestCase(1, 0)]
        [TestCase(-1, 10)]
        [TestCase(0, 0)]
        [TestCase(5, 0)]
        public void StringGenerator_GenerateEvenOddPairs_Should_ThrowArgumentOutOfRangeException_When_ArgumentsAreNegativeOrZero(int pairCount, int max)
        {
            // arrange
            // act
            // assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => stringGenerator.GenerateEvenOddPairs(pairCount, max));
        }
    }
}