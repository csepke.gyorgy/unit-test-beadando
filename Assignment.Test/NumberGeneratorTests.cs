﻿namespace Assignment.Test
{
    using Assignment.Interfaces;
    using Assignment.Numbers;
    using NUnit.Framework;
    using System;

    [TestFixture]
    public class NumberGeneratorTests
    {
        INumberGenerator numberGenerator;

        [OneTimeSetUp]
        public void Setup()
        {
            this.numberGenerator = new NumberGenerator();
        }

        [TestCase(0)]
        [TestCase(6)]
        [TestCase(7)]
        [TestCase(1000)]
        [TestCase(1001)]
        [TestCase(int.MaxValue - 1)]
        public void NumberGenerator_GenerateEven_Should_ReturnNonNegativeEvenNumber_When_LimitIsNotNegative(int limit)
        {
            // arrange
            // act
            int result = this.numberGenerator.GenerateEven(limit);

            // assert
            Assert.GreaterOrEqual(result, 0);
            Assert.LessOrEqual(result, limit);
            Assert.That(result % 2 == 0);
        }

        [TestCase(-1)]
        [TestCase(-1000)]
        [TestCase(-100000)]
        [TestCase(int.MinValue)]
        public void NumberGenerator_GenerateEven_Should_ThrowArgumentOutOfRangeException_When_LimitIsNegative(int limit)
        {
            // arrange
            // act
            // assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numberGenerator.GenerateEven(limit));
        }

        [TestCase(1)]
        [TestCase(6)]
        [TestCase(7)]
        [TestCase(1000)]
        [TestCase(1001)]
        [TestCase(int.MaxValue - 1)]
        public void NumberGenerator_GenerateOdd_Should_ReturnNonNegativeOddNumber_When_LimitIsPositive(int limit)
        {
            // arrange

            // act
            int result = numberGenerator.GenerateOdd(limit);

            // assert
            Assert.GreaterOrEqual(result, 0);
            Assert.LessOrEqual(result, limit);
            Assert.That(result % 2 == 1);
        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-1000)]
        [TestCase(-100000)]
        [TestCase(int.MinValue)]
        public void NumberGenerator_GenerateOdd_Should_ThrowArgumentOutOfRangeException_When_LimitIsNegativeOrZero(int limit)
        {
            // arrange
            // act
            // assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numberGenerator.GenerateOdd(limit));
        }
    }
}
