﻿namespace Assignment.Test
{
    using Assignment.Interfaces;
    using Assignment.Strings;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class StringUtilsTests
    {
        IStringUtils stringUtils;

        [SetUp]
        public void Setup()
        {
            stringUtils = new StringUtils();
        }

        [TestCase("strts")]
        [TestCase("strrts")]
        [TestCase("abba")]
        [TestCase("aba")]
        [TestCase("ababa")]
        public void StringUtils_IsPalindrom_Should_ReturnTrue_When_StrIsPalindrom(string str)
        {
            // arrange

            // act
            bool result = stringUtils.IsPalindrom(str);

            // assert
            Assert.IsTrue(result);
        }

        [TestCase("strtsssss")]
        [TestCase("strrrsrts")]
        [TestCase("abbab")]
        [TestCase("abaz")]
        [TestCase("abbaaba")]
        public void StringUtils_IsPalindrom_Should_ReturnFalse_When_StrIsNotPalindrom(string str)
        {
            // arrange

            // act
            bool result = stringUtils.IsPalindrom(str);

            // assert
            Assert.IsFalse(result);
        }

        [TestCase("\t\t")]
        [TestCase("")]
        [TestCase(" ")]
        [TestCase("  ")]
        [TestCase(null)]
        public void StringUtils_IsPalindrom_Should_ThrowArgumentException_When_StrIsNullOrWhiteSpace(string str)
        {
            // arrange
            // act
            // assert
            Assert.Throws(typeof(ArgumentException), () => stringUtils.IsPalindrom(str));
        }
    }
}