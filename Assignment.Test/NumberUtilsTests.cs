﻿namespace Assignment.Test
{
    using Assignment.Interfaces;
    using Assignment.Numbers;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class NumberUtilsTests
    {
        INumberUtils numberUtils;

        [SetUp]
        public void Setup()
        {
            numberUtils = new NumberUtils();
        }

        [TestCase(1, new object[] { 1 })]
        [TestCase(2, new object[] { 1, 2 })]
        [TestCase(3, new object[] { 1, 3 })]
        [TestCase(4, new object[] { 1, 2, 4 })]
        [TestCase(5, new object[] { 1, 5 })]
        [TestCase(6, new object[] { 1, 2, 3, 6 })]
        [TestCase(10, new object[] { 1, 2, 5, 10 })]
        [TestCase(15, new object[] { 1, 3, 5, 15 })]
        [TestCase(23, new object[] { 1, 23 })]
        [TestCase(30, new object[] { 1, 2, 3, 5, 6, 10, 15, 30 })]
        [TestCase(31, new object[] { 1, 31 })]
        [TestCase(100, new object[] { 1, 2, 4, 5, 10, 20, 25, 50, 100 })]
        public void NumberUtils_GetDivisors_Should_ReturnDivisors_When_NumberIsPositive(int number, object[] divisors)
        {
            // arrange
            List<int> divisorsList = divisors.Select(n => int.Parse(n.ToString())).ToList();

            // act
            List<int> result = numberUtils.GetDivisors(number);

            // assert
            Assert.That(result.All(l => divisorsList.Contains(l)));
            Assert.That(result.Count == divisorsList.Count);
        }

        [TestCase(0)]
        public void NumberUtils_GetDivisors_Should_ThrowArgumentException_When_NumberIsZero(int number)
        {
            // arrange
            // act
            // assert
            Assert.Throws(typeof(ArgumentException), () => numberUtils.GetDivisors(number));
        }

        [TestCase(-1)]
        [TestCase(-100)]
        [TestCase(-14320)]
        public void NumberUtils_GetDivisors_Should_ThrowArgumentOutOfRangeException_When_NumberIsNegative(int number)
        {
            // arrange
            // act
            // assert
            Assert.Throws(typeof(ArgumentOutOfRangeException), () => numberUtils.GetDivisors(number));
        }

        [TestCase(2)]
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        [TestCase(11)]
        [TestCase(13)]
        [TestCase(17)]
        [TestCase(19)]
        [TestCase(23)]
        [TestCase(29)]
        [TestCase(31)]
        [TestCase(37)]
        [TestCase(41)]
        [TestCase(43)]
        [TestCase(47)]
        [TestCase(53)]
        [TestCase(59)]
        [TestCase(61)]
        [TestCase(67)]
        [TestCase(71)]
        public void NumberUtils_IsPrime_Should_ReturnTrue_When_NumberIsPrime(int number)
        {
            // arrange
            // act
            bool result = numberUtils.IsPrime(number);

            // assert
            Assert.IsTrue(result);
        }

        [TestCase(-29)]
        [TestCase(-13)]
        [TestCase(-10)]
        [TestCase(-5)]
        [TestCase(-4)]
        [TestCase(-3)]
        [TestCase(-2)]
        [TestCase(-1)]
        [TestCase(1)]
        [TestCase(4)]
        [TestCase(6)]
        [TestCase(8)]
        [TestCase(9)]
        [TestCase(10)]
        [TestCase(14)]
        [TestCase(16)]
        [TestCase(18)]
        [TestCase(15)]
        [TestCase(20)]
        [TestCase(25)]
        public void NumberUtils_IsPrime_Should_ReturnFalse_When_NumberIsNotPrime(int number)
        {
            // arrange
            // act
            bool result = numberUtils.IsPrime(number);

            // assert
            Assert.IsFalse(result);
        }

        [TestCase(-10)]
        [TestCase(-4)]
        [TestCase(-2)]
        [TestCase(0)]
        [TestCase(2)]
        [TestCase(4)]
        [TestCase(10)]
        [TestCase(20)]
        public void NumberUtils_EvenOrOdd_Should_ReturnEven_When_NumberIsEven(int number)
        {
            // arrange
            // act
            string result = numberUtils.EvenOrOdd(number);

            // assert
            Assert.That(result == "even");
        }

        [TestCase(-11)]
        [TestCase(-5)]
        [TestCase(-3)]
        [TestCase(1)]
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(11)]
        [TestCase(21)]
        public void NumberUtils_EvenOrOdd_Should_ReturnOdd_When_NumberIsOdd(int number)
        {
            // arrange
            // act
            string result = numberUtils.EvenOrOdd(number);

            // assert
            Assert.That(result == "odd");
        }
    }
}
