﻿namespace Assignment.Interfaces
{
    public interface INumberGenerator
    {
        int GenerateEven(int limit);
        int GenerateOdd(int limit);
    }
}