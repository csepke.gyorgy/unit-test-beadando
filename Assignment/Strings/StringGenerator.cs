﻿namespace Assignment.Strings
{
    using Assignment.Interfaces;
    using System;
    using System.Collections.Generic;

    //public class StringGenerator
    public class StringGenerator : IStringGenerator
    {
        //private readonly NumberGenerator _numberGenerator;
        private readonly INumberGenerator _numberGenerator;

        //public StringGenerator(NumberGenerator numberGenerator)
        public StringGenerator(INumberGenerator numberGenerator)
        {
            _numberGenerator = numberGenerator;
        }

        /// <summary>
        /// Generates pairs of even and odd numbers.
        /// </summary>
        /// <param name="pairCount">Number of items to generate.</param>
        /// <param name="max">Maximum value of numbers generated.</param>
        /// <returns>A list of strings, containing an even and an odd positive number, separated by a comma (,). 
        /// All numbers are in the range of [0..max]</returns>
        public List<string> GenerateEvenOddPairs(int pairCount, int max)
        {
            if (pairCount < 0)
            {
                throw new ArgumentOutOfRangeException("Paircount cannot be negative.");
            }
            if (max < 1)
            {
                throw new ArgumentOutOfRangeException("Max cannot be negative or zero");
            }

            var EvenOddPairs = new List<string>();

            //for (int i = 1; i < pairCount; i++)
            for (int i = 0; i < pairCount; i++)
            {
                var element = _numberGenerator.GenerateEven(max) + "," + _numberGenerator.GenerateOdd(max);
                EvenOddPairs.Add(element);
            }

            return EvenOddPairs;
        }
    }
}
