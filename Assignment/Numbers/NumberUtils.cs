﻿namespace Assignment.Numbers
{
    using Assignment.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    //public class NumberUtils
    public class NumberUtils : INumberUtils
    {
        /// <summary>
        /// Gets all divisors for the number passed in.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>Retuns a list of all the divisors for the number passed in.</returns>
        public List<int> GetDivisors(int number)
        {
            if (number == 0)
                throw new ArgumentException("There are endless solutions.");
            if (number < 0)
                throw new ArgumentOutOfRangeException("Number cannot be negative.");

            // var divisors = new List<int>();
            List<int> divisors = new List<int>() { 1, number };

            int sqrt = (int)Math.Sqrt(number);
            for (int i = 2; i <= sqrt; i++)
            {
                if(number % i == 0)
                {
                    divisors.Add(i);
                    divisors.Add(number / i);
                }
            }

            // return divisors;
            return divisors.Distinct().ToList();
        }

        /// <summary>
        /// Checks if the number passed in is a prime number or not.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>True if the number is prime, False if it is not a prime number.</returns>
        public bool IsPrime(int number)
        {
            return number < 2 ? false : GetDivisors(number).Count() == 2;
            //return number < 0 ? false : GetDivisors(number).Count() == 2;
        }

        /// <summary>
        /// Checks if the number passed in is even or odd.
        /// </summary>
        /// <param name="number"></param>
        /// <returns>"even" if the number is even, "odd" in case of odd numbers.</returns>
        public string EvenOrOdd(int number)
        {
            return number % 2 == 0 ? "even" : "odd";
            //return number % 2 == 1 ? "even" : "odd";
        }
    }
}